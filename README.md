# How to use?
1. Run script
> python3 -m pip install virtualenv && python3 -m virtualenv .venv && source .venv/bin/activate && pip install ansible && export ANSIBLE_HOST_KEY_CHECKING=False

2. Add file with user pub key to roles/pubkey-adder/files
> on example look to devops.pub

3. Run ansible key adder
> ansible-playbook -i inventory.yml playbook.yml
